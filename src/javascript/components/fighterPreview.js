import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const { name, health, attack, defense } = fighter;
  const info = [name, health, attack, defense];
  const infoName = ['Name', 'Health', 'Attack', 'Defense'];
  const fighterImage = createFighterImage(fighter);
  const fighterDescription = createElement({ tagName: 'ul', className: 'fighter-preview-info-list' });
  for(let i = 0; i < info.length; i++ ){
    const element = createElement({ tagName: 'li'});
    element.innerText = `${infoName[i]} : ${info[i]}`;
    fighterDescription.appendChild(element);
  }
  fighterElement.append(fighterImage, fighterDescription);

  
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
