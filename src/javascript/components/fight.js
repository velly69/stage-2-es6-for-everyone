import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const playerControls = controls;
  let { health: healthFirst } = firstFighter;
  let { health : healthSecond} = secondFighter;
  let currentFunction = ' ';
  return new Promise((resolve) => {
    runOnKeys(
      getDamage,
      "KeyA",
      "KeyL"
    );
    runOnKeys(
      getDamage,
      "KeyJ",
      "KeyD"
    );
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const hit = getHitPower(attacker);
  const block = getBlockPower(defender);
  const damage = hit - block;
  if(damage < 0) {
    console.log(damage);
    return 0;
  }
  else {
    console.log(damage);
    return damage;
  }
  // return damage
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  let criticalHitChance = getRandom(1,2);
  const power = attack * criticalHitChance;
  return power;
  // return hit power
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  let dodgeChance = getRandom(1,2);
  const power = defense * dodgeChance;
  return power;
  // return block power
}

function getRandom(min, max){
  return Math.random() * (max - min) + min;
}


function runOnKeys(func, ...codes) {
  let pressed = new Set();
  document.addEventListener('keydown', (event) => {
    pressed.add(event.code);
    for (let code of codes) { 
      if (!pressed.has(code)) {
        return;
      }
    }
    func();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });
}

runOnKeys(
  function(){
    alert("Привет!")
  },
  "KeyQ",
  "KeyW"
);

//   document.addEventListener('keydown', (event) =>{
//     for(let [key, value] of Object.entries(playerControls)){
//       if(event.code === value){
//         currentFunction = key;
//       }
//     }
//     switch(currentFunction){
//       case 'PlayerOneAttack': getHitPower(firstFighter);
//         currentFunction = '';
//         break;
//       case 'PlayerOneBlock': getBlockPower(firstFighter);
//       currentFunction = '';
//         break;
//       case 'PlayerTwoAttack' : getHitPower(secondFighter);
//       currentFunction = '';
//         break;
//       case 'PlayerTwoBlock' : getBlockPower(secondFighter);
//       currentFunction = '';
//         break;
//}
  